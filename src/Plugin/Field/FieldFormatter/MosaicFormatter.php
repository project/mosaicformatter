<?php

namespace Drupal\mosaicformatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Cache\Cache;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
/**
 * Plugin implementation of the 'mosaicformatter' formatter.
 *
 * @FieldFormatter(
 *   id = "mosaicformatter",
 *   label = @Translation("Mosaic from multiple image field"),
 *   field_types = {
 *     "image",
 *   }
 * )
 */
class MosaicFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The image style entity storage.
   *
   * @var \Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;

   /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

/**
   * Constructs an ImageFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
   *   The image style storage.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, EntityStorageInterface $image_style_storage, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
    $this->entityTypeManager = $entity_type_manager;
  }


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        "image_style"=>"",
        "maxRowHeight"=>400,
        "refitOnResize"=>1,
        "refitOnResizeDelay"=>"",
        "defaultAspectRatio"=>1,
        "maxRowHeightPolicy"=>'skip',
        "maxRows"=>"",
        "highResImagesWidthThreshold"=>350,
        "outerMargin"=>0,
        "innerGap"=>0,
        "responsiveWidthThreshold"=>"",
        "maxItemsToShowWhenResponsiveThresholdSurpassed"=>"",
        "showTailWhenNotEnoughItemsForEvenOneRow"=>"",
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $description_link = Link::fromTextAndUrl(
      $this->t('Configure Image Styles'),
      Url::fromRoute('entity.image_style.collection')
    );
    $elements['image_style'] = [
      '#title' => $this->t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => $this->t('None (original image)'),
      '#options' => $image_styles,
      '#description' => $description_link->toRenderable() + [
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ],
    ];
    $elements['maxRowHeight'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Maximum row height'),
      '#default_value' => $this->getSetting('maxRowHeight'),
      '#description' => $this->t('The maximum desired height of rows'),
      '#size' => 10,
    ];
    $elements['refitOnResize'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Refit on resize'),
      '#default_value' => $this->getSetting('refitOnResize'),
      '#description' => $this->t('Whether to rebuild the mosaic when the window is resized or not'),
    ];
    $elements['refitOnResizeDelay'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Refit on resize delay'),
      '#default_value' => $this->getSetting('refitOnResizeDelay'),
      '#description' => $this->t("Milliseconds to wait after a resize event to refit the mosaic. Useful when creating huge mosaics that can take some CPU time on the user's browser. Leave it to false to refit the mosaic in realtime"),
      '#size' => 10,
    ];
    $elements['defaultAspectRatio'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Default aspect ratio'),
      '#default_value' => $this->getSetting('defaultAspectRatio'),
      '#description' => $this->t("The aspect ratio to use when none has been specified, or can't be calculated"),
      '#size' => 10,
    ];
    $elements['maxRowHeightPolicy'] = [
      '#type' => 'select',
      '#title' => $this->t('Max row height policy'),
      '#default_value' => $this->getSetting("maxRowHeightPolicy"),
      '#options'=>["skip"=>"skip","crop"=>"crop","oversize"=>"oversize","tail"=>"tail"],
      '#description' => $this->t('Sometimes some of the remaining items cannot be fitted on a row without surpassing the maxRowHeight. For those cases, choose one of the available settings for maxRowHeightPolicy:
          <br><strong>skip</strong> Does not renders the unfitting items.
          <br><strong>crop</strong> Caps the desired height to the specified maxRowHeight, resulting in those items not keeping their aspect ratios.
          <br><strong>oversize</strong> Renders items respecting their aspect ratio but surpassing the specified maxRowHeight.
          <br><strong>tail</strong> Renders items respecting their aspect ratio without surpassing the specified maxRowHeight, resulting in a last row that might not completely fit the screen horizontally.'),
    ];
    $elements['maxRows'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Maximum number of rows'),
      '#default_value' => $this->getSetting('maxRows'),
      '#description' => $this->t('If specified the mosaic will not have more than this number of rows'),
      '#size' => 10,
    ];
    $elements['highResImagesWidthThreshold'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('High res image threshold'),
      '#default_value' => $this->getSetting('highResImagesWidthThreshold'),
      '#description' => $this->t('The item width on which to start using the the provided high resolution image instead of the normal one.'),
      '#size' => 10,
    ];
    $elements['outerMargin'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Outer margin'),
      '#default_value' => $this->getSetting('outerMargin'),
      '#description' => $this->t('A margin size in pixels for the outher edge of the whole mosaic.'),
      '#size' => 10,
    ];
    $elements['innerGap'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Inner gap'),
      '#default_value' => $this->getSetting('innerGap'),
      '#description' => $this->t('A gap size in pixels to leave a space between elements.'),
      '#size' => 10,
    ];
    $elements['responsiveWidthThreshold'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Responsive width threshold'),
      '#default_value' => $this->getSetting('responsiveWidthThreshold'),
      '#description' => $this->t('The minimum width for which to keep building the mosaic. If specified, when the width is less than this, the mosaic building logic is not applied, and one item per row is always shown. This might help you avoid resulting item sizes that are too small and might break complex html/css inside them, specially when aiming for great responsive mosaics. When using this, you can also specify a "data-only-force-height-when-necessary" html item property with value "1" in the specific items you don\'t want to apply forced aspect ratios when this minimum width threshold is reached.'),
    ];
    $elements['maxItemsToShowWhenResponsiveThresholdSurpassed'] = [
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
       ),
      '#title' => $this->t('Max items to show when responsive threshold is surpassed'),
      '#default_value' => $this->getSetting('maxItemsToShowWhenResponsiveThresholdSurpassed'),
      '#description' => $this->t('If set (and also responsiveWidthThreshold is set), only this amount of items will be shown when the responsiveWidthThreshold is met.'),
      '#size' => 10,
    ];
    $elements['showTailWhenNotEnoughItemsForEvenOneRow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show tail when not enough items to fill last row'),
      '#default_value' => $this->getSetting('showTailWhenNotEnoughItemsForEvenOneRow'),
      '#description' => $this->t('If this is set to true, when there are not enough items to fill even a single row, they will be shown anyway even if they do not complete the row horizontally. If left to false, no mosaic will be shown in such occasions.'),
      '#size' => 10,
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $settings = $this->getSettings();
    $defaults=$this->defaultSettings();
    foreach($settings as $settingname=>$value) {
        if ($defaults[$settingname]!=$value && $value!="" && $settingname!="image_style") {
            $summary[]=$settingname." ".$value;
        }
    }

    $image_styles = image_style_options(FALSE);
    // Unset possible 'No defined styles' option.
    unset($image_styles['']);
    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
      $summary[] = $this->t('Image style: @style', ['@style' => $image_styles[$image_style_setting]]);
    }
    else {
      $summary[] = $this->t('Original image');
    }


    return $summary;
  }



public static function exploreobject($object,$array=true,$depth=0) {
    $maxdepth=3;
    $depth++;
    if (gettype($object)=="array") {
        if ($depth<$maxdepth) {
            foreach($object as $key=>$value) {
                $result[$key]=exploreobject($value,$array,$depth);
            }
            if ($array)
                return $result;
            else
                return print_r($result,true);
        } else {
            return "(array) MAX_DEPTH_REACHED";
        }
    } else
     if (gettype($object)=="object") {
        if ($array)
            return ["(object) ".get_class($object)=>["methods"=>get_class_methods($object),"properties"=>get_object_vars($object)]];
        else
            return print_r(["(object) ".get_class($object)=>["methods"=>get_class_methods($object),"properties"=>get_object_vars($object)]],true);
    } else {
        return "(".gettype($object).") ".print_r($object,true);
    }
}



  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $settings = $this->getSettings();
    $cache_tags[] = "node:" . $items->getEntity()->id();
    //$files = $this->getEntitiesToView($items, $langcode);
    $style = NULL;
    if (strlen($settings['image_style']) > 0 && isset($settings['image_style'])) {
      $style = $this->imageStyleStorage->load($settings['image_style']);
      $cache_tags = $style->getCacheTags();
    }

    $elements['#theme'] = 'mosaicformatter';
    $elements['#field_name'] = $items->getName();
    $defaultsettings = $this->defaultSettings();
    $settingstosend = [];
    foreach ($settings as $settingname => $value) {
      if ($defaultsettings[$settingname] != $value && $value != "" && $settingname != "image_style") {
        $settingstosend[$settingname] = $value;
      }
    }
    $elements['#style_name'] = $settings['image_style'];
    $elements['#settings'] = json_encode($settingstosend);

    foreach ($items as $delta => $item) {
      $img = $item->getValue();
      $file = $this->entityTypeManager->getStorage('file')
        ->load($img["target_id"]);
      $cache_tags = Cache::mergeTags($cache_tags, $file->getCacheTags());
      $urlBase = \Drupal::service('file_url_generator')
        ->generate($file->getFileUri())
        ->toUriString();
      $url = Url::fromUri(
        $urlBase, ['absolute' => TRUE]
      )->toString();

      if ($style !== NULL) {
        $url = $style->buildUrl($file->getFileUri());
        $imgdims = getimagesize($url);
        if ($imgdims !== FALSE) {
          $img["width"] = $imgdims[0];
          $img["height"] = $imgdims[1];
        }
      }

      $url = \Drupal::service('file_url_generator')->transformRelative($url);

      $elements['#items'][$delta] = [
        'uri' => $url,
        'width' => $img["width"],
        'height' => $img["height"],
        'alt' => $img["alt"],
        'title' => $img["title"],
      ];
    }

    $elements['#cache'] = [
      'tags' => $cache_tags,
    ];

    $elements["#attached"] = [
      'library' => [
        'mosaicformatter/mosaic',
      ],
    ];
    return $elements;
  }

}
