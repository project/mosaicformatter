jQuery(document).ready(function ($) { 
    $('.mosaicformattercontainer').each(function() {
        $(this).css("display","block");
        var settings=JSON.parse($(this).attr("data-settings"));
        for (var prop in settings) {
            if (Object.prototype.hasOwnProperty.call(settings, prop)) {
                if (typeof settings[prop] ==="string") {
                    try {
                        settings[prop]=parseInt(settings[prop])
                    } catch(e) {                        
                    }
                }                
            }
        }        
        $(this).Mosaic(settings);
    });
});